package com.example.jsonhomework

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.activity_main.*
import org.json.JSONObject

class MainActivity : AppCompatActivity() {
    private val userList = mutableListOf<UserModel.Data>()
    private lateinit var adapter: UsersViewAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        init()
        getUsers()
    }
    private fun getUsers() {
        HttpRequest.getRequest(HttpRequest.USERS, object : CustomCallback {
            override fun onFailure(body: String) {
            }

            override fun onResponse(body: String) {
                parseJson()

            }
        })
    }

    private fun init() {
        adapter = UsersViewAdapter(userList)
        recyclerView.layoutManager = LinearLayoutManager(this)
        recyclerView.adapter = adapter

        swipeRefreshLayout.setOnRefreshListener {
            swipeRefreshLayout.isRefreshing = true
            refresh()

            Handler().postDelayed({
                swipeRefreshLayout.isRefreshing = false
                parseJson()
                adapter.notifyDataSetChanged()
            }, 2500)
        }

    }

    private fun refresh() {
        userList.clear()
        adapter.notifyDataSetChanged()
    }


    private val json =
        "{page\":2,\"per_page\":6,\"total\":12,\"total_pages\":2,\"data\":[{\"id\":7,\"email\":\"michael.lawson@reqres.in\",\"first_name\":\"Michael\",\"last_name\":\"Lawson\",\"avatar\":\"https://s3.amazonaws.com/uifaces/faces/twitter/follettkyle/128.jpg\"},{\"id\":8,\"email\":\"lindsay.ferguson@reqres.in\",\"first_name\":\"Lindsay\",\"last_name\":\"Ferguson\",\"avatar\":\"https://s3.amazonaws.com/uifaces/faces/twitter/araa3185/128.jpg\"},{\"id\":9,\"email\":\"tobias.funke@reqres.in\",\"first_name\":\"Tobias\",\"last_name\":\"Funke\",\"avatar\":\"https://s3.amazonaws.com/uifaces/faces/twitter/vivekprvr/128.jpg\"},{\"id\":10,\"email\":\"byron.fields@reqres.in\",\"first_name\":\"Byron\",\"last_name\":\"Fields\",\"avatar\":\"https://s3.amazonaws.com/uifaces/faces/twitter/russoedu/128.jpg\"},{\"id\":11,\"email\":\"george.edwards@reqres.in\",\"first_name\":\"George\",\"last_name\":\"Edwards\",\"avatar\":\"https://s3.amazonaws.com/uifaces/faces/twitter/mrmoiree/128.jpg\"},{\"id\":12,\"email\":\"rachel.howell@reqres.in\",\"first_name\":\"Rachel\",\"last_name\":\"Howell\",\"avatar\":\"https://s3.amazonaws.com/uifaces/faces/twitter/hebertialmeida/128.jpg\"}],\"ad\":{\"company\":\"StatusCode Weekly\",\"url\":\"http://statuscode.org/\",\"text\":\"A weekly newsletter focusing on software development, infrastructure, the server, performance, and the stack end of things.\"}}"

    private fun parseJson() {

        val userModel = UserModel()
        val json = JSONObject(json)

        if (json.has("page")) {
            userModel.page = json.getInt("page")
        }
        if (json.has("per_page")) {
            userModel.perPage = json.getInt("per_page")
        }
        if (json.has("total")) {
            userModel.total = json.getInt("total")
        }
        if (json.has("total_pages")) {
            userModel.totalPages = json.getInt("total_pages")
        }

        for (i in 0 until json.getJSONArray("data").length()) {
            val jsonObject = json.getJSONArray("data")[i] as JSONObject
            val data = UserModel.Data()
            data.id = jsonObject.getInt("id")
            data.email = jsonObject.getString("email")
            data.firstName = jsonObject.getString("first_name")
            data.lastName = jsonObject.getString("last_name")
            data.avatar = jsonObject.getString("avatar")
            userModel.data.add(data)
            userList.add(data)
        }


    }

}