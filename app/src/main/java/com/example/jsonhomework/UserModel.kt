package com.example.jsonhomework


class UserModel(
    var page: Int = 0,
    var perPage: Int = 0,
    var total: Int = 0,
    var totalPages: Int = 0
) {
    var data: MutableList<Data> = mutableListOf()
    var add: String = ""
    var url: String = ""
    var text: String = ""

    class Data(
        var id: Int = 0,
        var email: String = "",
        var firstName: String = "",
        var lastName: String = "",
        var avatar: String = ""
    )
}